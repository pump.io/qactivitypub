##  QActivityPub demo client
##  Copyright 2017  JanKusanagi JRR <jancoding@gmx.com>
##
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
##
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
##
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
##

QT       += core gui widgets network

TARGET = democlient
TEMPLATE = app

message("Building demo client with Qt v$$QT_VERSION")

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += src/main.cpp \
    src/mainwindow.cpp

HEADERS  += src/mainwindow.h



### TEMP
INCLUDEPATH += ..
LIBS += -L../../build -lqactivitypub
