/*
 * This file is part of QActivityPub
 * Copyright 2017  JanKusanagi JRR <jancoding@gmx.com>
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setWindowTitle("QActivityPub Demo Client");
    setWindowIcon(QIcon::fromTheme("view-conversation-balloon"));
    setMinimumSize(500, 600);

    m_ApController = new QActivityPub(this);


    m_InfoTextEdit = new QTextEdit("Demo client started.",  this);
    m_InfoTextEdit->setReadOnly(true);

    setCentralWidget(m_InfoTextEdit);


    m_InfoTextEdit->append(QString("Using QActivityPub v%1.")
                                  .arg(m_ApController->getVersion()));

    qDebug() << "Main window created";
}

MainWindow::~MainWindow()
{
    qDebug() << "Main window destroyed";
}
