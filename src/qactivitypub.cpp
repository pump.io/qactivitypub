/*
 * This file is part of QActivityPub
 * Copyright 2017  JanKusanagi JRR <jancoding@gmx.com>
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "qactivitypub.h"


QActivityPub::QActivityPub(QObject *parent) : QObject(parent)
{
    m_LibraryVersion = "0.1-dev";
    m_UserAgentString = "QActivityPub/" + m_LibraryVersion.toLocal8Bit();


    connect(&m_NetworkAccessManager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(onRequestFinished(QNetworkReply*)));

    qDebug() << "QActivityPub initialized";
}

/**
 * @brief Get library version string
 *
 */
const QString QActivityPub::getVersion()
{
    return m_LibraryVersion;
}

/**
 * @brief Set the User Agent string to be used in server requests (optional)
 *
 * If this is not called, "QActivityPub/$VERSION" will be used
 *
 */
void QActivityPub::setUserAgentString(QByteArray ua)
{
    m_UserAgentString = ua;
}

/**
 * @brief Gives current user ID
 *
 */
QString QActivityPub::getUserId()
{
    return m_UserId;
}

/**
 * @brief Set the user ID to be used
 *
 * You need to call this method at startup or after your program asks the user
 * to enter their address for the first time
 *
 * @param newUserId
 *
 */
void QActivityPub::setUserId(QString newUserId)
{
    m_UserId = newUserId;

    emit userIdChanged(m_UserId);
}

/**
 * @brief Ask server for our user profile information
 *
 */
void QActivityPub::requestProfile()
{

}

/**
 * @brief Ask server for a specific feed, like inbox or outbox
 *
 */
void QActivityPub::requestFeed(QString id)
{

}


///////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// SLOTS ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


void QActivityPub::onRequestFinished(QNetworkReply *reply)
{

}

