/*
 * This file is part of QActivityPub
 * Copyright 2017  JanKusanagi JRR <jancoding@gmx.com>
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef QACTIVITYPUB_H
#define QACTIVITYPUB_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QSslError>
#include <QNetworkProxy>
#include <QTimer>

// JSON parsing
#include <QVariant>
#include <QVariantMap>
#include <QJsonDocument>

// For OAuth authentication
//#include <QtOAuth> // FUTURE, if we go with QOAuth

#include <QDebug>

#include "qactivitypub_global.h"


class QACTIVITYPUBSHARED_EXPORT QActivityPub : public QObject
{
    Q_OBJECT

public:
    QActivityPub(QObject *parent);

    const QString getVersion();
    void setUserAgentString(QByteArray ua);

    QString getUserId();
    void setUserId(QString newUserId);

    void requestProfile();
    void requestFeed(QString id);


signals:
    /// Emitted when a new user ID is set, usually at startup
    void userIdChanged(QString newUserId);

    /// Emitted when the user's profile is received
    void profileReceived();

    /// Emitted when a feed is received
    void feedReceived(QString id);


public slots:
    void onRequestFinished(QNetworkReply *reply);


private:
    QNetworkAccessManager m_NetworkAccessManager;
    QString m_LibraryVersion;
    QByteArray m_UserAgentString;


    // API data
    QString m_ClientId;
    QString m_ClientSecret;
    QByteArray m_Token;
    QByteArray m_TokenSecret;

    const QString m_ApiPublicTarget = "https://www.w3.org/ns"
                                      "/activitystreams#Public";
    QString m_ApiBaseUrl;
    QString m_ApiInboxUrl;
    QString m_ApiOutboxUrl;


    // User data
    QString m_UserId;
    int m_FollowingCount;
    int m_FollowersCount;

};


#endif // QACTIVITYPUB_H
